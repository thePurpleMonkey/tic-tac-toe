import socket, pygame, pygbutton, eztext, os
from pygame.locals import *

assets = {}
options = {"music": True, "sounds": True}
screen = None
FONT = pygame.font.Font('AkashiMF.ttf', 24)
focus = True

def mainloop():
	global focus

	events = pygame.event.get()
	for event in events:
		if event.type == QUIT:
			pygame.quit()
			exit()
		elif event.type == ACTIVEEVENT:
			if event.state == 6:
				focus = True
			elif event.state == 2:
				focus = False

	return events

def play_sound_if_not_focus(sound):
	global focus

	if not focus and options["sounds"]:
		assets[sound].play()

def draw_X():
	result = pygame.Surface((100, 100))
	result.fill((255, 0, 255))
	result.set_colorkey((255, 0, 255))
	pygame.draw.line(result, (0, 0, 0), (10, 10), (90, 90), 3)
	pygame.draw.line(result, (0, 0, 0), (10, 90), (90, 10), 3)
	return result

def draw_O():
	result = pygame.Surface((100, 100))
	result.fill((255, 0, 255))
	result.set_colorkey((255, 0, 255))
	pygame.draw.circle(result, (0, 0, 0), (50, 50), 40, 2)
	return result

def draw_board(board):
	screen.blit(assets["background"], screen.get_rect())
	pygame.draw.line(screen, (0, 0, 0), (100, 0), (100, 300), 3)
	pygame.draw.line(screen, (0, 0, 0), (200, 0), (200, 300), 3)
	pygame.draw.line(screen, (0, 0, 0), (0, 100), (300, 100), 3)
	pygame.draw.line(screen, (0, 0, 0), (0, 200), (300, 200), 3)

	for i in range(9):
		if board[i] == 1:
			screen.blit(draw_X(), ((i%3)*100, (i/3)*100))
		elif board[i] == 2:
			screen.blit(draw_O(), ((i%3)*100, (i/3)*100))

	pygame.display.update()

def find_opponent():
	s = socket.socket()
	s.setblocking(False)
	s.bind(("", 4040))
	s.listen(1)

	conn, addr = None, None

	text = render_message("Waiting for opponnent.\nPlease be patient...")
	cancel = pygbutton.PygButton((100, 180, 100, 40), "Cancel", font=FONT)

	screen.blit(assets["background"], screen.get_rect())
	screen.blit(text, (150-text.get_width()/2, 100-text.get_height()/2))
	cancel.draw(screen)

	pygame.display.update()

	while True:
		for event in mainloop():
			if "click" in cancel.handleEvent(event):
				return None

		try:
			conn, addr = s.accept()
		except socket.error as e:
			if e[0] == 10035:
				pass
			else:
				raise
		else:
			play_sound("opponent_found")
			return conn

def play_sound(sound):
	if options["sounds"]:
		assets[sound].play()

def connect_to_host():
	host_input = eztext.Input(font=FONT, prompt="Hostname: ", restricted="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_:/.")
	cancel = pygbutton.PygButton((100, 180, 100, 40), "Cancel", font=FONT)
	loop = True

	while loop:
		screen.blit(assets["background"], screen.get_rect())

		textinputsurface = host_input.get_surface()
		screen.blit(textinputsurface, (150-textinputsurface.get_width()/2, 150-textinputsurface.get_height()/2))
		cancel.draw(screen)

		pygame.display.update()

		for event in mainloop():
			if event.type == KEYDOWN and event.key in (K_KP_ENTER, K_RETURN):
				host_address = host_input.value
				loop = False
			else:
				host_input.update([event])

			if "click" in cancel.handleEvent(event):
				return None

	screen.blit(assets["background"], screen.get_rect())
	text = render_message("Connecting to {}...\nPlease be patient.".format(host_address))
	screen.blit(text, (150-text.get_width()/2, 150-text.get_height()/2))
	pygame.display.update()

	try:
		s = socket.socket()
		s.connect((host_address, 4040))
		s.setblocking(False)
		return s
	except socket.gaierror as e:
		back = pygbutton.PygButton((110, 180, 80, 40), "Back", font=FONT)
		screen.blit(assets["background"], screen.get_rect())
		text = render_message("Unable to resolve hostname.\n(Did you spell it right?)")
		screen.blit(text, (150-text.get_width()/2, 150-text.get_height()/2))
		back.draw(screen)
		pygame.display.update()

		while True:
			for event in mainloop():
				if "click" in back.handleEvent(event):
					return None
				pygame.display.update(back.rect)

	except socket.error as e:
		fatal_error(e[1])

def render_message(messages):
	surfaces = []
	height = 0
	max_width = 0

	# temp = messages.split('\n')
	# messages = []
	# for message in temp:
	# 	while len(message) > 20:
	# 		messages.append(message[:20])
	# 		message = message[20:]
	# 	messages.append(message)

	for message in messages.split('\n'):
		surf = FONT.render(message, True, (0, 0, 0))
		surf.set_colorkey((255, 0, 255))
		surfaces.append(surf)
		height += surf.get_height() + 5
		if surf.get_width() > max_width:
			max_width = surf.get_width()

	result = pygame.Surface((max_width, height), pygame.SRCALPHA, 32)
	result.convert_alpha()

	top = 0
	for surface in surfaces:
		result.blit(surface, (max_width/2-surface.get_width()/2, top))
		top += surface.get_height() + 5

	return result

def fatal_error(messages):
	print messages
	text = render_message(messages)

	screen.blit(assets["background"], screen.get_rect())
	screen.blit(text, (150-(text.get_width()/2), 150-(text.get_height()/2)))

	pygame.display.update()

	while True:
		mainloop()

def make_move(board, mode):
	while True:
		draw_board(board)

		for event in mainloop():
			if event.type == MOUSEBUTTONUP:
				x, y = event.pos
				row, col = y/100, x/100
				index = row*3 + col
				if board[index] == 0:
					board[index] = mode
					return

def find_winner(board):
    # Given a board, this function returns a number if that player has won.
    if board[6] == board[7] == board[8]: return board[6] # across the top
    if board[3] == board[4] == board[5]: return board[3] # across the middle
    if board[0] == board[1] == board[2]: return board[0] # across the bottom
    if board[6] == board[3] == board[0]: return board[6] # down the left side
    if board[7] == board[4] == board[1]: return board[7] # down the middle
    if board[8] == board[5] == board[2]: return board[8] # down the right side
    if board[6] == board[4] == board[2]: return board[6] # diagonal
    if board[8] == board[4] == board[0]: return board[8] # diagonal

def receive_move(s):
	result = []

	while True:
		mainloop()

		try:
			for char in s.recv(1024).split():
				result += [int(char)]
			
			if result == []:
				fatal_error("Opponent has disconnected.")	
				play_sound_if_not_focus("disconnect")

			play_sound_if_not_focus("notify")

			return result
		except socket.error as e:
			if e[0] == 10035:
				pass
			else:
				fatal_error(e[1])

def send_move(board, s):
	result = ""
	for place in board:
		result += str(place) + " "

	s.sendall(result)

def tie(board):
	return board[0] != 0 and board[1] != 0 and board[2] != 0 and board[3] != 0 and board[4] != 0 and board[5] != 0 and board[6] != 0 and board[7] != 0 and board[8] != 0

def load_assets():
	assets["background"] = pygame.image.load("img/background.jpg").convert()
	assets["background"] = pygame.transform.scale(assets["background"], (300, 300))

	assets["opponent_found"] = pygame.mixer.Sound("snd/opponent_found.ogg")
	assets["notify"]		 = pygame.mixer.Sound("snd/notify.ogg")
	assets["disconnect"]	 = pygame.mixer.Sound("snd/disconnect.ogg")

	pygame.mixer.music.load("snd/background.ogg")
	if options["music"]: pygame.mixer.music.play(-1)
	pygame.mixer.music.set_volume(.5)

def toggle_option(option):
	options[option] = not options[option]

def options_screen():
	music = pygbutton.PygButton((20, 130, 120,  40), "Music", font=FONT)
	sounds = pygbutton.PygButton((160, 130, 120, 40), "Sounds", font=FONT)
	back  = pygbutton.PygButton((100, 200, 100, 40), "Back", font=FONT)

	while True:
		screen.blit(assets["background"], screen.get_rect())
		music.draw(screen)
		sounds.draw(screen)
		back.draw(screen)

		pygame.display.update()

		for event in mainloop():
			if "click" in music.handleEvent(event):
				toggle_option("music")
				if options["music"]:
					pygame.mixer.music.play(-1)
				else:
					pygame.mixer.music.stop()
			elif "click" in sounds.handleEvent(event):
				toggle_option("sounds")
			elif "click" in back.handleEvent(event):
				return

def get_mode():
	host_rect = pygame.Rect(60, 130, 80, 40)
	join_rect = pygame.Rect(160, 130, 80, 40)
	options_rect = pygame.Rect(100, 200, 100, 40)

	host = pygbutton.PygButton(host_rect, "Host", font=FONT)
	join = pygbutton.PygButton(join_rect, "Join", font=FONT)
	options = pygbutton.PygButton(options_rect, "Options", font=FONT)

	while True:
		screen.blit(assets["background"], screen.get_rect())
		host.draw(screen)
		join.draw(screen)
		options.draw(screen)

		pygame.display.update()

		for event in mainloop():
			if "click" in host.handleEvent(event):
				return 1
			elif "click" in join.handleEvent(event):
				return 2
			elif "click" in  options.handleEvent(event):
				options_screen()

def endgame(winner, mode):
	replay_button = pygbutton.PygButton(pygame.Rect(60, 180, 80, 40), "Replay", font=FONT)
	quit_button   = pygbutton.PygButton(pygame.Rect(160, 180, 80, 40), "Quit", font=FONT)
	text = ""

	if winner == None:
		text = "Tie. Meh."
	elif winner == mode:
		text = "You won! Congratulations!"
	else:
		text = "You lost. Try harder."

	surface = FONT.render(text, True, (0, 0, 0))

	while True:
		screen.blit(assets["background"], screen.get_rect())
		screen.blit(surface, (150-surface.get_width()/2, 150-surface.get_height()/2))
		replay_button.draw(screen)
		quit_button.draw(screen)

		pygame.display.update()

		for event in mainloop():
			if "click" in quit_button.handleEvent(event):
				return False
			elif "click" in replay_button.handleEvent(event):
				return True

if __name__ == "__main__":
	s = None
	mode = None
	repeat = True

	pygame.init()

	screen = pygame.display.set_mode((300, 300))
	pygame.display.set_caption("Multiplayer Tic Tac Toe")

	load_assets()

	while s == None:
		mode = get_mode()

		if mode == 1:
			s = find_opponent()
		elif mode == 2:
			s = connect_to_host()

	play_sound("opponent_found")

	while repeat:
		board = [0] * 9
		draw_board(board)

		if mode == 2:
			make_move(board, mode)
			send_move(board, s)

		while True:
			draw_board(board)
			board = receive_move(s)
			if find_winner(board) or tie(board):
				repeat = endgame(find_winner(board), mode)
				break
			make_move(board, mode)
			send_move(board, s)
			draw_board(board)
			if find_winner(board) or tie(board):
				repeat = endgame(find_winner(board), mode)
				break