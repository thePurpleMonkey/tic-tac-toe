import socket

def board_to_string(board):
	result = ""
	for place in board:
		result += str(place) + " "

	return result

def get_visual_representaion(square):
	if square == 1:
		return "X"
	elif square == 2:
		return "O"
	elif square == 0:
		return " "

def draw_board(board):
    # This function prints out the board that it was passed.
    print '   |   |'
    print ' ' + get_visual_representaion(board[0]) + ' | ' + get_visual_representaion(board[1]) + ' | ' + get_visual_representaion(board[2])
    print '   |   |'
    print '-----------'
    print '   |   |'
    print ' ' + get_visual_representaion(board[3]) + ' | ' + get_visual_representaion(board[4]) + ' | ' + get_visual_representaion(board[5])
    print '   |   |'
    print '-----------'
    print '   |   |'
    print ' ' + get_visual_representaion(board[6]) + ' | ' + get_visual_representaion(board[7]) + ' | ' + get_visual_representaion(board[8])
    print '   |   |'

def host():
	s = socket.socket()
	s.bind(("", 4040))
	s.listen(1)

	conn, addr = s.accept()
	print "Connection from {} on port {}.".format(addr[0], addr[1])
	s.close()

	return conn, 1

def join():
	host = raw_input("Please enter host to connect to: ")
	s = socket.socket()
	s.connect((host, 4040))

	return s, 2

def make_move(board, mode):
	while True:
		draw_board(board)

		try:
			move = int(raw_input("Your move: ")) - 1
		except ValueError:
			print "That is not a nubmer. Please enter a number between 0 and 8."
		else:
			if move < 0 or move > 8:
				print "That number is too big or small. Please enter a number between 0 and 8."

			# Map old number system to new number system.
			# 3/17/2013
			if move >= 6 and move <= 8:
				move -= 6
			elif move >= 0 and move <= 2:
				move += 6

			print "index: {}".format(move)

			if board[move] == 0:
				board[move] = mode
				return
			else:
				print "That space has already been taken. Please choose another space."

def find_winner(board):
    # Given a board, this function returns a number if that player has won.
    if board[6] == board[7] == board[8]: return board[6] # across the top
    if board[3] == board[4] == board[5]: return board[3] # across the middle
    if board[0] == board[1] == board[2]: return board[0] # across the bottom
    if board[6] == board[3] == board[0]: return board[6] # down the left side
    if board[7] == board[4] == board[1]: return board[7] # down the middle
    if board[8] == board[5] == board[2]: return board[8] # down the right side
    if board[6] == board[4] == board[2]: return board[6] # diagonal
    if board[8] == board[4] == board[0]: return board[8] # diagonal

def receive_move(s):
	result = []

	for char in s.recv(1024).split():
		result += [int(char)]

	return result

def send_move(board, s):
	s.sendall(board_to_string(board))

def tie(board):
	return board[0] != 0 and board[1] != 0 and board[2] != 0 and board[3] != 0 and board[4] != 0 and board[5] != 0 and board[6] != 0 and board[7] != 0 and board[8] != 0

if __name__ == "__main__":
	response = ""
	s = None
	board = [0] * 9
	mode = None

	while response != 'j' and response != 'h':
		response = raw_input("Would you like to Host or Join a game? ")[0].lower()
		if response == 'j':
			s, mode = join()
			make_move(board, mode)
			send_move(board, s)
		elif response == 'h':
			s, mode = host()

	while True:
		board = receive_move(s)
		if find_winner(board):
			print "Winner is {}".format("you!" if find_winner(board) == mode else "somebody else.")
			break
		if tie(board):
			print "Tie. Get a life."
			break
		make_move(board, mode)
		send_move(board, s)
		if find_winner(board):
			print "Winner is {}".format("you!" if find_winner(board) == mode else "somebody else.")
			break
		if tie(board):
			print "Tie. Get a life."
			break